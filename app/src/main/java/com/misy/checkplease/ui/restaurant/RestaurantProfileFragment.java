package com.misy.checkplease.ui.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.misy.checkplease.R;
import com.misy.checkplease.api.restaurant.UpdateRestaurantRequest;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.Restaurant;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by stav.raviv on 6/7/2015.
 */
public class RestaurantProfileFragment extends BaseFragment implements View.OnClickListener, RequestListener<UpdateRestaurantRequest.Response> {
    public static final String TAG = "RestaurantProfileFragment";

    private EditText editName;
    private EditText editAddress;
    private EditText editPhone;
    private Spinner spinnerCity;
    private Button buttonSave;

    private Restaurant restaurant;

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restaurant = getGlobalData().getManagedRestaurant();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_restaurant_profile, container, false);

        getViewReferences(root);
        setupCityChooser();
        populateFields();

        return root;
    }

    private void populateFields() {
        editName.setText(restaurant.name);
        editAddress.setText(restaurant.address);
        editPhone.setText(restaurant.phoneNumber);

        int index = -1;
        for (int i = 0; i < spinnerCity.getCount(); i++) {
            if (spinnerCity.getItemAtPosition(i).equals(restaurant.city)) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            spinnerCity.setSelection(index);
        }
    }

    private void getViewReferences(View root) {
        editName = findView(root, R.id.editName);
        editAddress = findView(root, R.id.editAddress);
        editPhone = findView(root, R.id.editPhone);
        spinnerCity = findView(root, R.id.spinnerCity);
        buttonSave = findView(root, R.id.buttonSave);

        buttonSave.setOnClickListener(this);
    }

    private void setupCityChooser() {
        String[] cities = getGlobalData().getAllCities();
        if (cities != null) {
            spinnerCity.setAdapter(new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_item,
                    cities));
        }
    }

    private void setAllComponentsEnabled(boolean enabled) {
        editName.setEnabled(enabled);
        editAddress.setEnabled(enabled);
        editPhone.setEnabled(enabled);
        spinnerCity.setEnabled(enabled);
        buttonSave.setEnabled(enabled);
    }

    @Override
    public void onClick(View v) {
        setAllComponentsEnabled(false);
        restaurant.name = editName.getText().toString();
        restaurant.address = editAddress.getText().toString();
        restaurant.phoneNumber = editPhone.getText().toString();
        restaurant.city = spinnerCity.getSelectedItem().toString();
        getApi().updateRestaurant(restaurant, this);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        Snackbar.make(getView(), "Network Error, Please try again", Snackbar.LENGTH_SHORT).show();
        buttonSave.setEnabled(true);
    }

    @Override
    public void onRequestSuccess(UpdateRestaurantRequest.Response response) {
        setAllComponentsEnabled(true);
        Snackbar.make(getView(), "Saved!", Snackbar.LENGTH_SHORT).show();
        restaurant = response.getData();
        getGlobalData().setManagedRestaurant(restaurant);
        populateFields();
    }
}
