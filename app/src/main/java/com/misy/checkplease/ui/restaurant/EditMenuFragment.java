package com.misy.checkplease.ui.restaurant;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.misy.checkplease.R;
import com.misy.checkplease.api.GetCategoriesRequest;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.GlobalData;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.ui.RestaurantMenuAdapter;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created on 6/6/15.
 */
public class EditMenuFragment extends BaseFragment implements RestaurantMenuAdapter.Callbacks, RequestListener<GetCategoriesRequest.Response> {
    public static final String TAG = "TempTabFragment";

    private ListView listView;
    private RestaurantMenuAdapter adapter;

    private BroadcastReceiver dataChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (GlobalData.ACTION_MANAGED_RESTAURANT_UPDATED.equals(action)) {
                setupListView();
            }
        }
    };

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_menu, container, false);

        getViewReferences(root);
        setupListView();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupListView();
        IntentFilter filter = new IntentFilter(GlobalData.ACTION_MANAGED_RESTAURANT_UPDATED);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dataChangedReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dataChangedReceiver);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showLoadingSpinner();
            getApi().getCategories(this);
        }
    }

    private void getViewReferences(View root) {
        listView = findView(root, R.id.listView);

        findView(root, R.id.buttonAddNewItem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditMenuItemDialogFragment.getInstance(null).show(getChildFragmentManager(), EditMenuFragment.TAG);
            }
        });
    }

    private void setupListView() {
        adapter = new RestaurantMenuAdapter(getGlobalData().getManagedRestaurant(), getActivity());
        adapter.setCallbacks(this);
        adapter.setIsEditMode(true);
        listView.setAdapter(adapter);
    }

    @Override
    public void onActionButtonClicked(MenuItem menuItem) {
        EditMenuItemDialogFragment.getInstance(menuItem.id).show(getChildFragmentManager(), EditMenuFragment.TAG);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
    }

    @Override
    public void onRequestSuccess(GetCategoriesRequest.Response response) {
        hideLoadingSpinner();
        getGlobalData().setCategories(response.getData());
    }
}
