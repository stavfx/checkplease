package com.misy.checkplease.ui.cutomer;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.misy.checkplease.R;
import com.misy.checkplease.api.customer.OrderDishesRequest;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.GlobalData;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.model.Order;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created on 30/5/15.
 */
public class RestaurantCheckoutFragment extends BaseFragment implements View.OnClickListener, RequestListener<OrderDishesRequest.Response> {
    private static final String TAG = "RestaurantCheckoutFragment";

    private CheckoutAdapter adapter;
    private TextView textTotal;
    private TextView textNextOrder;

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_restaurant_checkout, container, false);

        textTotal = findView(root, R.id.textTotal);
        textNextOrder = findView(root, R.id.textNextOrder);
        ListView list = findView(root, R.id.listView);
        adapter = new CheckoutAdapter();
        list.setAdapter(adapter);

        findView(root, R.id.btn_submit_order).setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        showLoadingSpinner();
        getApi().orderDishes(getGlobalData().getCurrentRestaurantID(), getGlobalData().getTableNumber(), getGlobalData().getPendingOrder(), this);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
        Snackbar.make(getView(), "order failed, try again", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestSuccess(OrderDishesRequest.Response response) {
        hideLoadingSpinner();
        getGlobalData().getPendingOrder().clear();
        getGlobalData().getCompletedOrders().add(response.getData());
        getActivity().supportInvalidateOptionsMenu();
        adapter.updateData();
    }

    private class CheckoutAdapter extends BaseAdapter {
        private ArrayList<MenuItem> pendingItems = new ArrayList<>();
        private ArrayList<MenuItem> completedItems = new ArrayList<>();

        public CheckoutAdapter() {
            updateData();
        }

        public void updateData() {
            pendingItems.clear();
            completedItems.clear();

            GlobalData data = getGlobalData();
            pendingItems.addAll(data.getPendingOrder());
            for (Order order : data.getCompletedOrders()) {
                completedItems.addAll(Arrays.asList(order.menuItems));
            }

            updateTotals();

            notifyDataSetChanged();
        }

        private void updateTotals() {
            int sum = 0;
            for (MenuItem pendingItem : pendingItems) {
                sum += pendingItem.price;
            }
            textNextOrder.setText(getString(R.string.checkout_pending_subtotal, sum));

            for (MenuItem completedItem : completedItems) {
                sum += completedItem.price;
            }

            textTotal.setText(getString(R.string.checkout_total, sum));
        }

        @Override
        public int getCount() {
            return pendingItems.size() + completedItems.size();
        }

        @Override
        public MenuItem getItem(int position) {
            if (position < pendingItems.size()) {
                MenuItem item = pendingItems.get(position);
                item.isPending = true;
                return item;
            } else {
                MenuItem item = completedItems.get(position - pendingItems.size());
                item.isPending = false;
                return item;
            }
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_checkout, parent, false);

                holder.textName = findView(convertView, R.id.textName);
                holder.textComment = findView(convertView, R.id.textComment);
                holder.textPrice = findView(convertView, R.id.textPrice);
                holder.delete = findView(convertView, R.id.btnDelete);
                holder.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MenuItem item = (MenuItem) v.getTag();
                        getGlobalData().getPendingOrder().remove(item);
                        getActivity().supportInvalidateOptionsMenu();
                        updateData();
                    }
                });

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            MenuItem item = getItem(position);

            holder.delete.setTag(item);

            if (item.isPending) {
                holder.delete.setVisibility(View.VISIBLE);
                convertView.setBackgroundColor(getResources().getColor(R.color.white));
            } else {
                holder.delete.setVisibility(View.INVISIBLE);
                convertView.setBackgroundColor(getResources().getColor(R.color.primary_disabled));
            }

            holder.textName.setText(item.name);
            holder.textPrice.setText("₪" + item.price);

            if (TextUtils.isEmpty(item.comment)) {
                holder.textComment.setVisibility(View.GONE);
            } else {
                holder.textComment.setVisibility(View.VISIBLE);
                holder.textComment.setText(item.comment);
            }

            return convertView;
        }
    }

    private static class ViewHolder {
        TextView textName;
        TextView textComment;
        TextView textPrice;
        View delete;
    }
}
