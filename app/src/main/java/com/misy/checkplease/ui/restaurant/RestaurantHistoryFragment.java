package com.misy.checkplease.ui.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.misy.checkplease.R;
import com.misy.checkplease.api.restaurant.FeedResponse;
import com.misy.checkplease.app.BaseFragment;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created on 07/06/2015.
 */
public class RestaurantHistoryFragment extends BaseFragment implements RequestListener<FeedResponse> {
    public static final String TAG = "RestaurantHistoryFragment";

    private ListView listView;
    private FeedAdapter adapter;

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_restaurant_history, container, false);

        listView = findView(root, R.id.listView);
        listView.setAdapter(adapter = new FeedAdapter(getActivity()));

        return root;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showLoadingSpinner();
            getApi().getRestaurantHistory(this);
        }
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
    }

    @Override
    public void onRequestSuccess(FeedResponse feedResponse) {
        hideLoadingSpinner();
        adapter.setOrders(feedResponse.getData().dishes_orders);
    }
}
