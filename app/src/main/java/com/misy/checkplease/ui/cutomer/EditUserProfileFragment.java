package com.misy.checkplease.ui.cutomer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.misy.checkplease.R;
import com.misy.checkplease.api.auth.UpdateUserRequest;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.User;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created on 10/06/2015.
 */
public class EditUserProfileFragment extends BaseFragment implements View.OnClickListener, RequestListener<UpdateUserRequest.Response> {
    public static final String TAG = "EditUserProfileFragment";

    private EditText editEmail;
    private EditText editFirstName;
    private EditText editLastName;

    private User user;

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = getGlobalData().getCurrentUser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_user_profile, container, false);

        getViewReferences(root);
        populatedFields();

        return root;
    }

    private void getViewReferences(View root) {
        editEmail = findView(root, R.id.txt_email);
        editFirstName = findView(root, R.id.txt_first_name);
        editLastName = findView(root, R.id.txt_last_name);

        findView(root, R.id.buttonSave).setOnClickListener(this);
    }

    private void populatedFields() {
        editEmail.setText(user.id);
        editFirstName.setText(user.firstName);
        editLastName.setText(user.lastName);
    }

    @Override
    public void onClick(View v) {
        user.firstName = editFirstName.getText().toString();
        user.lastName = editLastName.getText().toString();
        showLoadingSpinner();
        getApi().updateUser(user, this);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
        Snackbar.make(getView(), "failed, please try again", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestSuccess(UpdateUserRequest.Response response) {
        hideLoadingSpinner();
        Snackbar.make(getView(), "Saved!", Snackbar.LENGTH_SHORT).show();
    }
}
