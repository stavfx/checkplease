package com.misy.checkplease.ui.cutomer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.zxing.integration.android.IntentIntegrator;
import com.misy.checkplease.R;
import com.misy.checkplease.app.BaseFragment;

/**
 * Created by stav.raviv on 5/17/2015.
 */
public class InputTableNumberFragment extends BaseFragment {
    public static final String TAG = "InputTableNumberFragment";
    public static final String EXTRA_CONTINUE_TO_CHECKOUT = "EXTRA_CONTINUE_TO_CHECKOUT";

    private EditText input;
    private boolean continueToCheckout;
    private View buttonSubmit;

    public static InputTableNumberFragment getInstance(boolean continueToCheckout) {
        InputTableNumberFragment fragment = new InputTableNumberFragment();
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_CONTINUE_TO_CHECKOUT, continueToCheckout);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            continueToCheckout = arguments.getBoolean(EXTRA_CONTINUE_TO_CHECKOUT, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_input_table_number, container, false);

        input = findView(root, R.id.txt_table_number);
        buttonSubmit = findView(root, R.id.btn_submit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTableNumber();
            }
        });
        findView(root, R.id.buttonScanQR).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = IntentIntegrator.forSupportFragment(InputTableNumberFragment.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.initiateScan();
            }
        });
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String scan_result = data.getStringExtra("SCAN_RESULT");
            if (!TextUtils.isEmpty(scan_result)) {
                input.setText(scan_result);
                buttonSubmit.post(new Runnable() {
                    @Override
                    public void run() {
                        buttonSubmit.performClick();
                    }
                });
            }
        }
    }

    private void saveTableNumber() {
        if (isInputValid()) {
            String tableCode = input.getText().toString();
            getGlobalData().setTableNumber(tableCode);

            Fragment parent = getParentFragment();
            if (parent instanceof RestaurantMasterFragment) {
                RestaurantMasterFragment master = (RestaurantMasterFragment) parent;
                master.getChildFragmentManager().popBackStack();
                if (continueToCheckout) {
                    master.innerNavigate(new RestaurantCheckoutFragment());
                }
            }
        } else {
            input.setError("Invalid Table Code");
        }
    }

    private boolean isInputValid() {
        String inputString = input.getText().toString();
        return !TextUtils.isEmpty(inputString) && inputString.length() == 5;
    }
}
