package com.misy.checkplease.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.misy.checkplease.R;
import com.misy.checkplease.api.GenericResponse;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.GlobalData;
import com.misy.checkplease.model.User;
import com.misy.checkplease.ui.cutomer.EditUserProfileFragment;
import com.misy.checkplease.ui.cutomer.UserHistoryFragment;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stav.raviv on 5/17/2015.
 */
public class DrawerFragment extends BaseFragment {
    public static final String TAG = "DrawerFragment";

    private static final String ITEM_MY_PROFILE = "My Profile";
    private static final String ITEM_HISTORY = "History";
    private static final String ITEM_LOGOUT = "Logout";

    private ListView listView;
    private BroadcastReceiver loginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshDrawerItems();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_drawer, container, false);
        listView = findView(root, R.id.listView);

        refreshDrawerItems();

        return listView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(GlobalData.ACTION_USER_LOGGED_IN);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(loginReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(loginReceiver);
    }

    private void refreshDrawerItems() {
        List<String> items = new ArrayList<>();
        if (getGlobalData().getCurrentUser().type == User.Type.CUSTOMER) {
            items.add(ITEM_MY_PROFILE);
            items.add(ITEM_HISTORY);
        }
        items.add(ITEM_LOGOUT);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                if (item.equals(ITEM_LOGOUT)) {
                    logout();
                } else if (item.equals(ITEM_MY_PROFILE)) {
                    getActivity().onBackPressed();
                    navigate(new EditUserProfileFragment());
                } else if (item.equals(ITEM_HISTORY)) {
                    getActivity().onBackPressed();
                    navigate(new UserHistoryFragment());
                }
            }
        });
    }

    private void logout() {
        showLoadingSpinner();

        getApi().logout(new RequestListener<GenericResponse>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                hideLoadingSpinner();
            }

            @Override
            public void onRequestSuccess(GenericResponse genericResponse) {
                hideLoadingSpinner();
                getGlobalData().setCurrentUser(null);
                getMainActivity().updateOrientation();
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                navigate(new MainFragment(), false);
            }
        });
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }
}
