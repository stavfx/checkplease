package com.misy.checkplease.ui;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.misy.checkplease.R;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.model.Order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 12/6/15.
 */
public abstract class BaseOrderAdapter extends BaseAdapter {
    private static class ViewHolder {
        TextView date;
        TextView textTitle;
        TextView textDetails;
        View buttonDone;
    }

    public interface Callbacks {
        void onDoneClicked(Order order);
    }

    private ArrayList<Order> orders = new ArrayList<>();
    protected Context context;
    private Callbacks callbacks;

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (callbacks != null) {
                Order order = (Order) v.getTag();
                callbacks.onDoneClicked(order);
            }
        }
    };

    public BaseOrderAdapter(Context context) {
        this.context = context;
    }

    protected boolean shouldShowActionButton(Order order) {
        return true;
    }

    protected abstract String getTitle(Order order);

    protected String getSubtitle(Order order) {
        StringBuilder sb = new StringBuilder();
        if (order.service != null) {
            sb.append(context.getString(order.service.getStringResource()));
        } else if (order.menuItems != null) {
            for (MenuItem menuItem : order.menuItems) {
                sb.append(menuItem.name);
                if (!TextUtils.isEmpty(menuItem.comment)) {
                    sb.append(" - ").append(menuItem.comment);
                }
                sb.append("\n");
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
        }
        return sb.toString();
    }

    public void setOrders(Order[] orders) {
        this.orders.clear();
        if (orders != null) {
            this.orders.addAll(Arrays.asList(orders));
            Collections.sort(this.orders);
        }
        notifyDataSetChanged();
    }

    public void setCallbacks(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Order getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_order_feed, parent, false);
            holder = new ViewHolder();

            holder.textTitle = findView(convertView, R.id.textTitle);
            holder.textDetails = findView(convertView, R.id.textDetails);
            holder.buttonDone = findView(convertView, R.id.buttonDone);
            holder.date = findView(convertView, R.id.textDate);
            holder.buttonDone.setOnClickListener(clickListener);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Order order = getItem(position);
        boolean isNewOrder = order.state == Order.State.NEW;

        int backgroundColor = isNewOrder ? R.color.white : R.color.primary_disabled;
        convertView.setBackgroundColor(context.getResources().getColor(backgroundColor));

        holder.textTitle.setText(getTitle(order));

        holder.textDetails.setText(getSubtitle(order));

        holder.date.setText(getOrderDate(order));

        holder.buttonDone.setVisibility(shouldShowActionButton(order) ? View.VISIBLE : View.GONE);
        holder.buttonDone.setTag(order);

        return convertView;
    }

    private String getOrderDate(Order order) {
        Date date = new Date(order.getDate());
        DateFormat dateFormat = new SimpleDateFormat("kk:mm - MMM d, yyyy", Locale.getDefault());
        return dateFormat.format(date);
    }

    protected <T extends View> T findView(View root, int resId) {
        if (root != null) {
            return (T) root.findViewById(resId);
        }
        return null;
    }
}
