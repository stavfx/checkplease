package com.misy.checkplease.ui.restaurant;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.misy.checkplease.R;
import com.misy.checkplease.api.restaurant.FeedResponse;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.Order;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.concurrent.TimeUnit;

/**
 * Created on 6/6/15.
 */
public class OrderFeedFragment extends BaseFragment implements RequestListener<FeedResponse>, FeedAdapter.Callbacks, View.OnClickListener {
    public static final String TAG = "OrderFeedFragment";

    private static final long REFRESH_INTERVAL = TimeUnit.SECONDS.toMillis(5);

    private ListView ordersList;
    private FeedAdapter ordersAdapter;
    private ListView servicesList;
    private FeedAdapter servicesAdapter;
    private Handler handler;
    private Runnable refreshFeedTask = new Runnable() {
        @Override
        public void run() {
            if (isAdded()) {
                getApi().getOrderFeed(OrderFeedFragment.this);
            }
        }
    };

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        getMainActivity().updateOrientation();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_order_feed, container, false);
        getViewReferences(root);

        ordersList.setAdapter(ordersAdapter = new FeedAdapter(getActivity()));
        ordersAdapter.setCallbacks(this);

        servicesList.setAdapter(servicesAdapter = new FeedAdapter(getActivity()));
        servicesAdapter.setCallbacks(this);

        findView(root, R.id.buttonArchive).setOnClickListener(this);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            showLoadingSpinner();
        }
        getApi().getOrderFeed(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(refreshFeedTask);
    }

    private void getViewReferences(View root) {
        ordersList = findView(root, R.id.ordersList);
        servicesList = findView(root, R.id.servicesList);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
        handler.postDelayed(refreshFeedTask, REFRESH_INTERVAL);

        if (getUserVisibleHint()) {
            Snackbar.make(getView(), "Connection Error. Trying again...", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestSuccess(FeedResponse response) {
        hideLoadingSpinner();
        handler.postDelayed(refreshFeedTask, REFRESH_INTERVAL);

        ordersAdapter.setOrders(response.getData().dishes_orders);
        servicesAdapter.setOrders(response.getData().services_orders);
    }

    @Override
    public void onDoneClicked(Order order) {
        order.state = Order.State.HANDLED;
        showLoadingSpinner();
        getApi().updateOrder(order, this);
    }

    @Override
    public void onClick(View v) {
        showLoadingSpinner();
        getApi().archiveCompletedOrders(this);
    }
}
