package com.misy.checkplease.ui.cutomer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.misy.checkplease.api.customer.GetUserHistoryRequest;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.Order;
import com.misy.checkplease.ui.BaseOrderAdapter;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created on 12/6/15.
 */
public class UserHistoryFragment extends BaseFragment implements RequestListener<GetUserHistoryRequest.Response> {
    public static final String TAG = "UserHistoryFragment";

    private ListView listView;
    private HistoryAdapter adapter;

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        listView = new ListView(getActivity());

        adapter = new HistoryAdapter(getActivity());
        listView.setAdapter(adapter);

        return listView;
    }

    @Override
    public void onResume() {
        super.onResume();
        showLoadingSpinner();
        getApi().getUserHistory(this);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
    }

    @Override
    public void onRequestSuccess(GetUserHistoryRequest.Response response) {
        hideLoadingSpinner();
        adapter.setOrders(response.getData());
    }

    private class HistoryAdapter extends BaseOrderAdapter {
        public HistoryAdapter(Context context) {
            super(context);
        }

        @Override
        protected String getTitle(Order order) {
            return getGlobalData().findRestaurantById(order.restaurantId).name;
        }

        @Override
        protected boolean shouldShowActionButton(Order order) {
            return false;
        }
    }
}
