package com.misy.checkplease.ui.cutomer;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.misy.checkplease.R;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.Restaurant;
import com.misy.checkplease.utils.BadgeDrawable;

/**
 * Created by stav.raviv on 5/11/2015.
 */
public class RestaurantMasterFragment extends BaseFragment {
    public static final String TAG = "RestaurantMasterFragment";

    private Restaurant restaurant;
    private TextView restaurantNameText;
    private TextView restaurantAddressText;
    private TextView restaurantPhoneText;
    private ImageView restaurantIcon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String restaurantId = getGlobalData().getCurrentRestaurantID();
        restaurant = getGlobalData().findRestaurantById(restaurantId);
        if (restaurant == null) {
            throw new IllegalArgumentException("can't find restaurant with id: " + restaurantId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_restaurant_master, container, false);

        getViewReferences(root);
        loadFirstFragment();

        restaurantNameText.setText(restaurant.name);
        restaurantPhoneText.setText(restaurant.phoneNumber);
        String address = "";
        if (!TextUtils.isEmpty(restaurant.address)) {
            address += restaurant.address + ", ";
        }
        if (!TextUtils.isEmpty(restaurant.city)) {
            address += restaurant.city;
        }
        restaurantAddressText.setText(address);

        if (!TextUtils.isEmpty(restaurant.icon)) {
            UrlImageViewHelper.setUrlDrawable(restaurantIcon, restaurant.icon);
        }

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getChildFragmentManager().findFragmentByTag(InputTableNumberFragment.TAG);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void innerNavigate(BaseFragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        if (addToBackStack) {
            ft.addToBackStack(fragment.getNavigationTag());
        }
        ft.replace(R.id.restaurant_inner_fragment_container, fragment, fragment.getNavigationTag());
        ft.commit();
    }

    public void innerNavigate(BaseFragment fragment) {
        innerNavigate(fragment, true);
    }

    private void loadFirstFragment() {
        innerNavigate(new RestaurantMenuListFragment(), false);
    }


    private void getViewReferences(View root) {
        restaurantNameText = findView(root, R.id.lbl_restaurant_name);
        restaurantNameText.setPaintFlags(restaurantNameText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        restaurantAddressText = findView(root, R.id.lbl_restaurant_address);
        restaurantPhoneText = findView(root, R.id.lbl_restaurant_telephone);
        restaurantIcon = findView(root, R.id.restaurant_icon);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.order_menu, menu);
        MenuItem item = menu.findItem(R.id.checkout);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        int pendingOrderSize = getGlobalData().getPendingOrder().size();
        if (pendingOrderSize > 0) {
            BadgeDrawable.setBadgeCount(icon, pendingOrderSize);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.checkout:
                checkout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkout() {
        if (isShowingMenuListFragment()) {
            if (getGlobalData().isTableSet()) {
                innerNavigate(new RestaurantCheckoutFragment());
            } else {
                innerNavigate(InputTableNumberFragment.getInstance(true));
            }
        }
    }

    private boolean isShowingMenuListFragment() {
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.restaurant_inner_fragment_container);
        return fragment instanceof RestaurantMenuListFragment;
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }
}
