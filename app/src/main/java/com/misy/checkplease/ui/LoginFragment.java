package com.misy.checkplease.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.misy.checkplease.R;
import com.misy.checkplease.api.auth.ConnectedResponse;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.User;
import com.misy.checkplease.ui.cutomer.SearchRestaurantFragment;
import com.misy.checkplease.ui.restaurant.RestaurantTabsFragment;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by stav.raviv on 5/13/2015.
 */
public class LoginFragment extends BaseFragment implements RequestListener<ConnectedResponse> {
    public static final String TAG = "LoginFragment";

    private EditText email;
    private EditText password;
    private Button loginButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        getViewReferences(root);
        setListeners();
        showKeyboard(email);
        return root;
    }

    private void getViewReferences(View root) {
        email = findView(root, R.id.txt_email);
        password = findView(root, R.id.txt_password);
        loginButton = findView(root, R.id.btn_login);
    }

    private void setListeners() {
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginButton.performClick();
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginClicked();
            }
        });
    }

    private void loginClicked() {
        hideKeyboard();
        showLoadingSpinner();
        getApi().login(email.getText().toString(), password.getText().toString(), this);
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    protected boolean hasNavigationDrawer() {
        return false;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
        Toast.makeText(getActivity(), "login failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestSuccess(ConnectedResponse response) {
        hideLoadingSpinner();
        getGlobalData().saveLoggedInResponse(response);
        getFragmentManager().popBackStack();
        if (response.getData().user.type == User.Type.CUSTOMER) {
            navigate(new SearchRestaurantFragment(), false);
        } else {
            navigate(new RestaurantTabsFragment());
        }
    }
}
