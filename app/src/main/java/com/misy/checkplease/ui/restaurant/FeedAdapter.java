package com.misy.checkplease.ui.restaurant;

import android.content.Context;

import com.misy.checkplease.R;
import com.misy.checkplease.model.Order;
import com.misy.checkplease.ui.BaseOrderAdapter;

/**
 * Created on 7/6/15.
 */
public class FeedAdapter extends BaseOrderAdapter {

    public FeedAdapter(Context context) {
        super(context);
    }

    @Override
    protected boolean shouldShowActionButton(Order order) {
        return order.state == Order.State.NEW;
    }

    @Override
    protected String getTitle(Order order) {
        return context.getString(R.string.table_number_header, order.tableNumber);
    }
}
