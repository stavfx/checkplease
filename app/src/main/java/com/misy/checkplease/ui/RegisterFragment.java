package com.misy.checkplease.ui;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.misy.checkplease.R;
import com.misy.checkplease.api.auth.ConnectedResponse;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.User;
import com.misy.checkplease.ui.cutomer.SearchRestaurantFragment;
import com.misy.checkplease.ui.restaurant.RestaurantTabsFragment;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created on 16/5/15.
 */
public class RegisterFragment extends BaseFragment implements RequestListener<ConnectedResponse> {
    private static final String TAG = "RegisterFragment";

    private EditText email;
    private EditText password;
    private EditText passwordConfirm;
    private EditText firstName;
    private EditText lastName;
    private Spinner userTypeSpinner;
    private Button registerButton;

    private UserTypesAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);
        getViewReferences(root);
        setListeners();
        showKeyboard(email);
        adapter = new UserTypesAdapter(getActivity());
        userTypeSpinner.setAdapter(adapter);
        return root;
    }

    private void getViewReferences(View root) {
        email = findView(root, R.id.txt_email);
        password = findView(root, R.id.txt_password);
        passwordConfirm = findView(root, R.id.txt_password_confirm);
        firstName = findView(root, R.id.txt_first_name);
        lastName = findView(root, R.id.txt_last_name);
        userTypeSpinner = findView(root, R.id.spinner_user_type);
        registerButton = findView(root, R.id.btn_register);
    }

    private void setListeners() {
        lastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    registerButton.performClick();
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }

    private void register() {
        if (isPasswordValid()) {
            hideKeyboard();
            showLoadingSpinner();
            User.Type type = getUserType();
            getApi().register(email.getText().toString(),
                    password.getText().toString(),
                    firstName.getText().toString(),
                    lastName.getText().toString(),
                    type,
                    this);
        }
    }

    private User.Type getUserType() {
        return adapter.getItem(userTypeSpinner.getSelectedItemPosition());
    }

    private boolean isPasswordValid() {
        String password = this.password.getText().toString();
        String passwordConfim = this.passwordConfirm.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getActivity(), "Password is empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!password.equals(passwordConfim)) {
            Toast.makeText(getActivity(), "Passwords don't  match", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    protected boolean hasNavigationDrawer() {
        return false;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
        Toast.makeText(getActivity(), "Registration failed :(", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestSuccess(ConnectedResponse response) {
        hideLoadingSpinner();
        Toast.makeText(getActivity(), "Registration completed", Toast.LENGTH_LONG).show();

        getGlobalData().saveLoggedInResponse(response);

        getFragmentManager().popBackStack();
        if (response.getData().user.type == User.Type.CUSTOMER) {
            navigate(new SearchRestaurantFragment(), false);
        } else {
            navigate(new RestaurantTabsFragment(), false);
        }
    }

    private class UserTypesAdapter extends ArrayAdapter<User.Type> {

        public UserTypesAdapter(Context context) {
            super(context, android.R.layout.simple_list_item_1, User.Type.validValues());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setText(getItem(position).getStringResource());
            return view;
        }
    }
}
