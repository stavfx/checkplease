package com.misy.checkplease.ui.restaurant;

import com.misy.checkplease.R;
import com.misy.checkplease.app.BaseFragment;

/**
 * Created on 6/6/15.
 */
public enum RestaurantsTabs {
    ORDERS(R.string.restaurant_tab_orders),
    HISTORY(R.string.restaurant_tab_history),
    MENU(R.string.restaurant_tab_menu),
    //    REPORTS(R.string.restaurant_tab_reports),
    PROFILE(R.string.restaurant_tab_profile);

    private int title;

    RestaurantsTabs(int title) {
        this.title = title;
    }

    public int getTitle() {
        return title;
    }

    public BaseFragment getFragment() {
        switch (this) {
            case ORDERS:
                return new OrderFeedFragment();
            case HISTORY:
                return new RestaurantHistoryFragment();
            case MENU:
                return new EditMenuFragment();
            case PROFILE:
                return new RestaurantProfileFragment();
//            case REPORTS:
//                break;
        }
        return null;
    }
}
