package com.misy.checkplease.ui.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.misy.checkplease.R;
import com.misy.checkplease.api.API;
import com.misy.checkplease.api.restaurant.UpdateRestaurantRequest;
import com.misy.checkplease.app.BaseDialogFragment;
import com.misy.checkplease.app.MainActivity;
import com.misy.checkplease.model.Category;
import com.misy.checkplease.model.GlobalData;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.model.Restaurant;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stav.raviv on 6/9/2015.
 */
public class EditMenuItemDialogFragment extends BaseDialogFragment implements View.OnClickListener, RequestListener<UpdateRestaurantRequest.Response> {
    public static final String TAG = "EditMenuItemDialogFragment";
    public static final String EXTRA_MENU_ITEM_ID = "EXTRA_MENU_ITEM_ID";

    private Restaurant restaurant;
    private MenuItem editedMenuItem;

    private EditText editName;
    private EditText editDescription;
    private EditText editPrice;
    private Spinner spinnerCategory;

    /**
     * @param menuItemId - item to edit, null for new item
     * @return
     */
    public static EditMenuItemDialogFragment getInstance(String menuItemId) {
        EditMenuItemDialogFragment fragment = new EditMenuItemDialogFragment();
        if (!TextUtils.isEmpty(menuItemId)) {
            Bundle args = new Bundle();
            args.putString(EXTRA_MENU_ITEM_ID, menuItemId);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restaurant = GlobalData.get(getActivity()).getManagedRestaurant();
        Bundle args = getArguments();
        if (args != null) {
            String id = args.getString(EXTRA_MENU_ITEM_ID);
            editedMenuItem = findMenuItemById(id);
        }
    }

    private MenuItem findMenuItemById(String id) {
        for (List<MenuItem> menuItems : restaurant.menu.values()) {
            for (MenuItem menuItem : menuItems) {
                if (menuItem.id.equals(id)) {
                    return menuItem;
                }
            }
        }
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dialog_edit_menu_item, container, false);

        String title;
        if (editedMenuItem == null) {
            title = "Add new item";
        } else {
            title = "Edit item";
        }
        getDialog().setTitle(title);

        getViewReferences(root);
        setupSpinner();
        populateFields();

        return root;
    }

    private void getViewReferences(View root) {
        editName = findView(root, R.id.editName);
        editDescription = findView(root, R.id.editDescription);
        editPrice = findView(root, R.id.editPrice);
        spinnerCategory = findView(root, R.id.spinnerCategory);

        findView(root, R.id.buttonSave).setOnClickListener(this);
        findView(root, R.id.buttonCancel).setOnClickListener(this);
    }

    private void setupSpinner() {
        Category[] categories = GlobalData.get(getActivity()).getCategories();
        spinnerCategory.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, categories));
    }

    private void populateFields() {
        if (editedMenuItem != null) {
            editName.setText(editedMenuItem.name);
            editDescription.setText(editedMenuItem.description);
            editPrice.setText(editedMenuItem.price + "");
            int index = getIndexOf(editedMenuItem.menu_category);
            if (index != -1) {
                spinnerCategory.setSelection(index);
            }
        }
    }

    private int getIndexOf(String category) {
        for (int i = 0; i < spinnerCategory.getCount(); i++) {
            if (spinnerCategory.getItemAtPosition(i).equals(category)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSave:
                showLoadingSpinner();
                if (editedMenuItem == null) {
                    saveNewItem();
                } else {
                    updateMenuItemFields();
                }
                API.getInstance(spiceManager).updateRestaurant(restaurant, this);
                break;
            case R.id.buttonCancel:
                dismiss();
                break;
        }
    }

    private void saveNewItem() {
        editedMenuItem = new MenuItem();
        updateMenuItemFields();

        if (restaurant.menu == null) {
            restaurant.menu = new HashMap<>();
        }

        List<MenuItem> menuItems = restaurant.menu.get(editedMenuItem.menu_category);
        if (menuItems == null) {
            menuItems = new ArrayList<>();
            restaurant.menu.put(editedMenuItem.menu_category, menuItems);
        }
        menuItems.add(editedMenuItem);
    }

    private void updateMenuItemFields() {
        editedMenuItem.name = editName.getText().toString();
        editedMenuItem.description = editDescription.getText().toString();
        editedMenuItem.price = Integer.parseInt(editPrice.getText().toString());
        String category = spinnerCategory.getSelectedItem().toString();
        editedMenuItem.menu_category = category;
    }

    protected void showLoadingSpinner() {
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.showLoadingSpinner();
        }
    }

    protected void hideLoadingSpinner() {
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.hideLoadingSpinner();
        }
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        hideLoadingSpinner();
        Snackbar.make(getView(), "Network error, try again", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestSuccess(UpdateRestaurantRequest.Response response) {
        hideLoadingSpinner();
        GlobalData.get(getActivity()).setManagedRestaurant(response.getData());
        dismiss();
    }
}
