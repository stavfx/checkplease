package com.misy.checkplease.ui.cutomer;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.misy.checkplease.R;
import com.misy.checkplease.api.customer.GetRestaurantsRequest;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.Restaurant;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by stav.raviv on 5/11/2015.
 */
public class SearchRestaurantFragment extends BaseFragment implements RequestListener<GetRestaurantsRequest.Response> {
    public static final String TAG = "SearchRestaurantFragment";

    private Spinner cityChooser;
    private ListView listRestaurants;
    private RestaurantsAdapter adapter;

    private String lastConstraint;
    private String lastCity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        getApi().getAllRestaurants(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search_restaurant, container, false);

        getViewReferences(root);

        setupCityChooser();
        setupListView();
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.search);
        if (getActivity() == null) {
            return;
        }
        SearchView sv = new SearchView(((ActionBarActivity) getActivity()).getSupportActionBar().getThemedContext());
        sv.setQueryHint("Search for restaurant...");
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // User clicked Search/Enter
                return true;
            }

            @Override
            public boolean onQueryTextChange(String constraint) {
                // New char typed
                lastConstraint = constraint.toLowerCase();
                filterResults();
                return false;
            }
        });
    }

    private void getViewReferences(View root) {
        cityChooser = findView(root, R.id.spinner_choose_city);
        listRestaurants = findView(root, R.id.list_restaurants);
    }

    private void setupCityChooser() {
        String[] cities = getGlobalData().getAllCities();
        if (cities != null) {
            List<String> list = new ArrayList<>(Arrays.asList(cities));
            list.add(0, "All");
            cityChooser.setAdapter(new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_list_item_1,
                    list));
            cityChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(final AdapterView<?> parent, View view, final int position, long id) {
                    if (position != 0) {
                        lastCity = parent.getAdapter().getItem(position).toString();
                    } else {
                        lastCity = null;
                    }
                    filterResults();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void filterResults() {
        List<Restaurant> restaurants = new ArrayList<>(Arrays.asList(getGlobalData().getAllRestaurants()));
        List<Restaurant> result = new ArrayList<>();
        for (Restaurant restaurant : restaurants) {
            boolean cityMatch = TextUtils.isEmpty(lastCity) || restaurant.city.equals(lastCity);
            boolean nameMatch = TextUtils.isEmpty(lastConstraint);
            if (cityMatch && !nameMatch) {
                nameMatch = isNameMatch(restaurant.name);
            }
            if (cityMatch && nameMatch) {
                result.add(restaurant);
            }
        }

        adapter.setItems(result);
    }

    private boolean isNameMatch(String restaurantName) {
        for (String nameSegment : restaurantName.toLowerCase().split(" ")) {
            if (!TextUtils.isEmpty(nameSegment) && nameSegment.startsWith(lastConstraint)) {
                return true;
            }
        }
        return false;
    }

    private void setupListView() {
        Restaurant[] restaurants = getGlobalData().getAllRestaurants();
        if (restaurants != null) {
            List<Restaurant> list = new ArrayList<>(Arrays.asList(restaurants));
            adapter = new RestaurantsAdapter(getActivity(), list);
            listRestaurants.setAdapter(adapter);
            listRestaurants.setOnItemClickListener(adapter);
        }
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        System.out.println();
    }

    @Override
    public void onRequestSuccess(GetRestaurantsRequest.Response response) {
        List<Restaurant> data = response.getData();
        Restaurant[] array = data.toArray(new Restaurant[data.size()]);
        getGlobalData().setAllRestaurants(array);
        setupListView();
    }

    private class RestaurantsAdapter extends ArrayAdapter<Restaurant> implements AdapterView.OnItemClickListener {

        public RestaurantsAdapter(Context context, List<Restaurant> objects) {
            super(context, R.layout.list_item_restaurant_search, objects);
        }

        public void setItems(List<Restaurant> restaurants) {
            clear();
            int i = 0;
            for (Restaurant restaurant : restaurants) {
                insert(restaurant, i++);
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_restaurant_search, parent, false);
                holder = new ViewHolder();

                holder.icon = findView(convertView, R.id.icon);
                holder.name = findView(convertView, R.id.lbl_restaurant_name);
                holder.details = findView(convertView, R.id.lbl_restaurant_details);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Restaurant restaurant = getItem(position);
            holder.name.setText(restaurant.name);

            String details = "";
            if (restaurant.address != null) {
                details += restaurant.address + ", ";
            }
            if (restaurant.city != null) {
                details += restaurant.city + ", ";
            }
            if (restaurant.phoneNumber != null) {
                details += restaurant.phoneNumber;
            }
            holder.details.setText(details);

            if (!TextUtils.isEmpty(restaurant.icon)) {
                UrlImageViewHelper.setUrlDrawable(holder.icon, restaurant.icon);
            } else {
                holder.icon.setImageResource(R.mipmap.ic_launcher);
            }

            return convertView;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            getGlobalData().setCurrentRestaurantID(getItem(position).id);
            navigate(new RestaurantMasterFragment());
        }
    }

    private static class ViewHolder {
        ImageView icon;
        TextView name;
        TextView details;
    }
}
