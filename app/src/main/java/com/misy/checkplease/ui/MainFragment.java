package com.misy.checkplease.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.misy.checkplease.R;
import com.misy.checkplease.app.BaseFragment;

public class MainFragment extends BaseFragment {

    private static final String TAG = "MainFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.login_button:
                        loginButtonClicked();
                        break;
                    case R.id.img_logo:
                        logoClicked();
                        break;
                    case R.id.register_button:
                        registerClicked();
                        break;
                }
            }
        };

        view.findViewById(R.id.img_logo).setOnClickListener(listener);
        view.findViewById(R.id.login_button).setOnClickListener(listener);
        view.findViewById(R.id.register_button).setOnClickListener(listener);
    }

    @Override
    protected boolean hasNavigationDrawer() {
        return false;
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    private void logoClicked() {
    }

    private void loginButtonClicked() {
        navigate(new LoginFragment());
    }

    private void registerClicked() {
        navigate(new RegisterFragment());
    }
}
