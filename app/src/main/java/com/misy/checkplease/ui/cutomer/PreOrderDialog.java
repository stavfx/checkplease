package com.misy.checkplease.ui.cutomer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.misy.checkplease.R;
import com.misy.checkplease.app.BaseDialogFragment;
import com.misy.checkplease.model.MenuItem;

import java.util.List;

/**
 * Created on 10/06/2015.
 */
public class PreOrderDialog extends BaseDialogFragment implements View.OnClickListener {
    public static final String TAG = "PreOrderDialog";
    public static final String EXTRA_MENU_ITEM_ID = "EXTRA_MENU_ITEM_ID";

    private TextView textName;
    private TextView textDescription;
    private EditText editComment;
    private MenuItem menuItem;

    public static PreOrderDialog getInstance(String menuItemId) {
        PreOrderDialog fragment = new PreOrderDialog();
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);

        if (!TextUtils.isEmpty(menuItemId)) {
            Bundle args = new Bundle();
            args.putString(EXTRA_MENU_ITEM_ID, menuItemId);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            String id = args.getString(EXTRA_MENU_ITEM_ID);
            menuItem = findMenuItemById(id);
        }

        if (menuItem == null) {
            throw new IllegalStateException("PreOrderDialog can't find the menu item");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dialog_pre_order, container, false);

        getViewReferences(root);
        populateFields();

        return root;
    }

    private void getViewReferences(View root) {
        textName = findView(root, R.id.textName);
        textDescription = findView(root, R.id.textDescription);
        editComment = findView(root, R.id.editComment);

        findView(root, R.id.buttonSave).setOnClickListener(this);
        findView(root, R.id.buttonCancel).setOnClickListener(this);
    }

    private void populateFields() {
        textName.setText(menuItem.name);
        textDescription.setText(menuItem.description);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSave:
                menuItem.comment = editComment.getText().toString();
                getGlobalData().getPendingOrder().add(menuItem);
                getActivity().supportInvalidateOptionsMenu();
                dismiss();
                break;
            case R.id.buttonCancel:
                dismiss();
                break;
        }
    }

    private MenuItem findMenuItemById(String id) {
        for (List<MenuItem> menuItems : getGlobalData().getCurrentRestaurant().menu.values()) {
            for (MenuItem menuItem : menuItems) {
                if (menuItem.id.equals(id)) {
                    return menuItem;
                }
            }
        }
        return null;
    }
}
