package com.misy.checkplease.ui.cutomer;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.misy.checkplease.R;
import com.misy.checkplease.api.GenericResponse;
import com.misy.checkplease.api.customer.GetRecommendedItemsRequest;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.model.InstantService;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.ui.RestaurantMenuAdapter;
import com.misy.checkplease.utils.PinnedSectionListView;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created on 30/5/15.
 */
public class RestaurantMenuListFragment extends BaseFragment implements RestaurantMenuAdapter.Callbacks, RequestListener<GetRecommendedItemsRequest.Response> {
    public static final String TAG = "RestaurantMenuListFragment";

    private PinnedSectionListView listView;
    private RestaurantMenuAdapter adapter;
    private FloatingActionMenu floatingActionMenu;
    private int previousVisibleItem;
    private InstantService pendingService;

    private View.OnClickListener menuClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InstantService service = (InstantService) v.getTag();
            if (getGlobalData().isTableSet()) {
                requestService(service);
            } else {
                pendingService = service;
                gotoInputTable();
            }
            floatingActionMenu.close(true);
        }
    };

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_restaurant_menu_list, container, false);

        getViewReferences(root);
        setupList();
        setupMenuButtons();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (pendingService != null && getGlobalData().isTableSet()) {
            requestService(pendingService);
        }
        pendingService = null;

        getApi().getRecommendedItems(getGlobalData().getCurrentRestaurantID(), this);
    }

    private void requestService(InstantService service) {
        String restaurantID = getGlobalData().getCurrentRestaurantID();
        String tableNumber = getGlobalData().getTableNumber();
        showLoadingSpinner();
        getApi().orderService(restaurantID, tableNumber, service, new RequestListener<GenericResponse>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                hideLoadingSpinner();
                Snackbar.make(getView(), "Request failed. Please try again.", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestSuccess(GenericResponse genericResponse) {
                hideLoadingSpinner();
            }
        });
    }

    private void getViewReferences(View root) {
        listView = findView(root, R.id.listView);
        floatingActionMenu = findView(root, R.id.floating_menu);
    }

    private void setupList() {
        adapter = new RestaurantMenuAdapter(getGlobalData().getCurrentRestaurant(), getActivity());
        adapter.setCallbacks(this);
        listView.setAdapter(adapter);
        listView.setShadowVisible(false);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > previousVisibleItem) {
                    floatingActionMenu.hideMenuButton(true);
                } else if (firstVisibleItem < previousVisibleItem) {
                    floatingActionMenu.showMenuButton(true);
                }
                previousVisibleItem = firstVisibleItem;
            }
        });
    }

    private void setupMenuButtons() {
        for (InstantService service : InstantService.values()) {
            FloatingActionButton fab = new FloatingActionButton(getActivity());
            fab.setTag(service);
            fab.setColorNormal(getResources().getColor(R.color.action_menu));
            fab.setColorPressed(getResources().getColor(R.color.action_menu_pressed));
            fab.setButtonSize(FloatingActionButton.SIZE_MINI);
            fab.setLabelText(getString(service.getStringResource()));
            fab.setImageResource(service.getIconResId());
            fab.setOnClickListener(menuClickListener);
            floatingActionMenu.addMenuButton(fab);
        }

        floatingActionMenu.setClosedOnTouchOutside(true);
        floatingActionMenu.setIconAnimated(false);
        floatingActionMenu.hideMenuButton(false);
        floatingActionMenu.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    floatingActionMenu.showMenuButton(true);
                    floatingActionMenu.setMenuButtonShowAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.show_from_bottom));
                    floatingActionMenu.setMenuButtonHideAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.hide_to_bottom));
                }
            }
        }, 300);
    }

    private void gotoInputTable() {
        Fragment parent = getParentFragment();
        if (parent instanceof RestaurantMasterFragment) {
            RestaurantMasterFragment master = (RestaurantMasterFragment) parent;
            master.innerNavigate(InputTableNumberFragment.getInstance(false));
        }
    }

    @Override
    public void onActionButtonClicked(MenuItem menuItem) {
        PreOrderDialog.getInstance(menuItem.id).show(getChildFragmentManager(), PreOrderDialog.TAG);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(GetRecommendedItemsRequest.Response response) {
        adapter.setRecommended(response.getData());
    }
}
