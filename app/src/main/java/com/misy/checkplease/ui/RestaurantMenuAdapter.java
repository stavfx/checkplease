package com.misy.checkplease.ui;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.misy.checkplease.R;
import com.misy.checkplease.model.Category;
import com.misy.checkplease.model.GlobalData;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.model.Restaurant;
import com.misy.checkplease.utils.PinnedSectionListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by stav.raviv on 6/9/2015.
 */
public class RestaurantMenuAdapter extends BaseAdapter implements PinnedSectionListView.PinnedSectionListAdapter {
    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_HEADER = 1;
    private static final int VIEW_TYPE_COUNT = 2;
    public static final Category CATEGORY_RECOMMENDED = new Category("Recommended for you", -1);

    public interface Callbacks {
        void onActionButtonClicked(MenuItem menuItem);
    }

    private static class HeaderViewHolder {
        TextView title;
    }

    private static class ViewHolder {
        TextView title;
        TextView description;
        TextView price;
        ImageView actionButton;
        View star;
    }

    private static class CategoryComparator implements Comparator<Category> {

        @Override
        public int compare(Category lhs, Category rhs) {
            return lhs.priority - rhs.priority;
        }
    }

    private Map<Category, List<MenuItem>> menu;
    private Context context;
    private Callbacks callbacks;
    private boolean isEditMode = false;

    public RestaurantMenuAdapter(Restaurant restaurant, Context context) {
        this.menu = new TreeMap<>(new CategoryComparator());
        List<Category> categories = Arrays.asList(GlobalData.get(context).getCategories());
        Category dummy = new Category();
        for (String key : restaurant.menu.keySet()) {
            dummy.id = key;
            int index = categories.indexOf(dummy);
            if (index > -1) {
                Category category = categories.get(index);
                this.menu.put(category, restaurant.menu.get(key));
            }
        }
        this.context = context;
    }

    public void setCallbacks(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void setRecommended(String[] recommendedIds) {
        ArrayList<MenuItem> result = new ArrayList<>();

        MenuItem template = new MenuItem();
        for (String id : recommendedIds) {
            template.id = id;
            if (result.contains(template)) {
                continue;
            }
            for (List<MenuItem> menuItems : menu.values()) {
                int index = menuItems.indexOf(template);
                if (index >= 0) {
                    MenuItem realItem = menuItems.get(index);
                    realItem.isRecommended = true;
                    result.add(realItem);
                    break;
                }
            }
        }
        if (result.size() > 0) {
            menu.put(CATEGORY_RECOMMENDED, result);
            notifyDataSetChanged();
        } else {
            menu.remove(CATEGORY_RECOMMENDED);
        }

        notifyDataSetChanged();
    }

    public void setIsEditMode(boolean isEditMode) {
        this.isEditMode = isEditMode;
    }

    @Override
    public int getCount() {
        int count = menu.keySet().size();
        for (List<MenuItem> categoryItems : menu.values()) {
            count += categoryItems.size();
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Category) {
            return VIEW_TYPE_HEADER;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    private List<MenuItem> getItemsForCategory(Category category) {
        return menu.get(category);
    }

    @Override
    public Object getItem(int position) {
        int count = 0;
        for (Category key : menu.keySet()) {
            if (position == count) {
                return key;
            }
            count++; // Point to first item in category

            List<MenuItem> items = getItemsForCategory(key);
            if (count + items.size() > position) {
                return items.get(position - count);
            }
            count += items.size(); // Point to next header index
        }
        throw new IllegalStateException("probably bad calculation of item index");
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) != VIEW_TYPE_HEADER;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            return getItemView(convertView, position);
        } else {
            return getHeaderView(convertView, position);
        }
    }

    private View getItemView(View convertView, int position) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(context, R.layout.list_item_restaurant_menu, null);
            holder.title = findView(convertView, R.id.lbl_food_item_title);
            holder.description = findView(convertView, R.id.lbl_food_item_subtitle);
            holder.price = findView(convertView, R.id.lbl_food_item_price);
            holder.star = findView(convertView, R.id.imgStar);
            holder.actionButton = findView(convertView, R.id.add_to_order_button);
            holder.actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callbacks != null) {
                        callbacks.onActionButtonClicked((MenuItem) v.getTag());
                    }
                }
            });

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        MenuItem item = (MenuItem) getItem(position);
        holder.actionButton.setTag(item);

        if (isEditMode) {
            holder.actionButton.setImageResource(android.R.drawable.ic_menu_edit);
        } else {
            holder.actionButton.setImageResource(R.drawable.fab_add);
        }

        String title = item.name;
        if (isInRecommendedSection(position)) {
            title += " (" + item.menu_category + ")";
        }
        holder.title.setText(title);

        if (item.isRecommended) {
            holder.star.setVisibility(View.VISIBLE);
        } else {
            holder.star.setVisibility(View.INVISIBLE);
        }

        holder.price.setText("₪" + item.price);

        if (!TextUtils.isEmpty(item.description)) {
            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(item.description);
        } else {
            holder.description.setVisibility(View.GONE);
        }

        return convertView;
    }

    private boolean isInRecommendedSection(int position) {
        List<MenuItem> items = menu.get(CATEGORY_RECOMMENDED);
        return items != null && position <= items.size();
    }

    private View getHeaderView(View convertView, int position) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = View.inflate(context, R.layout.list_header_restaurant_menu, null);
            holder.title = findView(convertView, R.id.lbl_header);
            if (holder.title != null) {
                holder.title.setPaintFlags(holder.title.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            }

            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        Category category = (Category) getItem(position);
        holder.title.setText(category.id);

        if (category.equals(CATEGORY_RECOMMENDED)) {
            holder.title.setBackgroundColor(context.getResources().getColor(R.color.action_menu));
        } else {
            holder.title.setBackgroundColor(context.getResources().getColor(R.color.primary_dark));
        }

        return convertView;
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == VIEW_TYPE_HEADER;
    }

    protected <T extends View> T findView(View root, int resId) {
        if (root != null) {
            return (T) root.findViewById(resId);
        }
        return null;
    }
}