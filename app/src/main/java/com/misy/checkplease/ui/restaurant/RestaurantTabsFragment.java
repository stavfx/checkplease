package com.misy.checkplease.ui.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.misy.checkplease.R;
import com.misy.checkplease.app.BaseFragment;
import com.misy.checkplease.utils.SlidingTabLayout;

/**
 * Created on 6/6/15.
 */
public class RestaurantTabsFragment extends BaseFragment {
    public static final String TAG = "RestaurantTabsFragment";

    @Override
    public String getNavigationTag() {
        return TAG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMainActivity().updateOrientation();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String url = getGlobalData().getManagedRestaurant().icon;
        if (!TextUtils.isEmpty(url)) {
            ImageView icon = getMainActivity().getTitleIcon();
            UrlImageViewHelper.setUrlDrawable(icon, url);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getMainActivity().getTitleIcon().setImageDrawable(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.viewpager, container, false);
        ViewPager viewPager = findView(root, R.id.viewpager);
        viewPager.setAdapter(new RestaurantTabsAdapter(getChildFragmentManager()));

        SlidingTabLayout slidingTabLayout = findView(root, R.id.sliding_tabs);
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary));
        slidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.white));
        slidingTabLayout.setHeaderTextColor(getResources().getColor(R.color.white));
        slidingTabLayout.setViewPager(viewPager);

        return root;
    }

    private class RestaurantTabsAdapter extends FragmentStatePagerAdapter {

        public RestaurantTabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return RestaurantsTabs.values().length;
        }

        private RestaurantsTabs getTab(int position) {
            return RestaurantsTabs.values()[position];
        }

        @Override
        public Fragment getItem(int i) {
            return getTab(i).getFragment();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(getTab(position).getTitle());
        }
    }
}
