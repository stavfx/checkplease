package com.misy.checkplease.model;

import com.google.gson.annotations.SerializedName;
import com.misy.checkplease.R;

/**
 * Created on 2/6/15.
 */
public enum InstantService {
    @SerializedName("check")
    CHECK_PLEASE("check", R.string.service_check_please, R.drawable.ic_bill),
    @SerializedName("waitress")
    WAITRESS("waitress", R.string.service_waitress, R.drawable.ic_raise_hand),
    @SerializedName("clean_table")
    CLEAN_TABLE("clean_table", R.string.service_clean_table, R.drawable.ic_clean_table),
    @SerializedName("water")
    WATER("water", R.string.service_water, R.drawable.ic_water),
    @SerializedName("condiments")
    CONDIMENTS("condiments", R.string.service_condiments, R.drawable.ic_condiments);

    private String id;
    private int stringResId;
    private int iconResId;

    InstantService(String id, int stringResId, int iconResId) {
        this.id = id;
        this.stringResId = stringResId;
        this.iconResId = iconResId;
    }

    public String getId() {
        return id;
    }

    public int getStringResource() {
        return stringResId;
    }

    public int getIconResId() {
        return iconResId;
    }
}