package com.misy.checkplease.model;

/**
 * Created on 6/6/15.
 */
public class MenuItem {
    public String id;
    public String name;
    public String description;
    public int price;
    public String menu_category;
    public boolean isRecommended;
    public String comment;
    public boolean isPending;

    @Override
    public boolean equals(Object o) {
        if (o instanceof MenuItem) {
            MenuItem another = (MenuItem) o;
            return this.id.equals(another.id);
        }
        return false;
    }
}
