package com.misy.checkplease.model;

import com.google.gson.annotations.SerializedName;
import com.misy.checkplease.R;

/**
 * Created on 9/5/15.
 */
public class User {
    public enum Type {
        UNDEFINED(0, R.string.user_type_undefined),
        @SerializedName("1")
        CUSTOMER(1, R.string.user_type_customer),
        @SerializedName("2")
        RESTAURANT(2, R.string.user_type_restaurant_admin);

        private int id;
        private int stringResId;

        public static Type[] validValues() {
            return new Type[]{CUSTOMER, RESTAURANT};
        }

        Type(int id, int stringResId) {
            this.id = id;
            this.stringResId = stringResId;
        }

        public int getId() {
            return id;
        }

        public int getStringResource() {
            return stringResId;
        }
    }

    public String id;
    public Type type = Type.UNDEFINED;
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    public String email;
}
