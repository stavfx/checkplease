package com.misy.checkplease.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created on 2/6/15.
 */
public class Restaurant implements Cloneable {

    public String id;
    public String name;
    public String city;
    public String address;
    @SerializedName("desc")
    public String description;
    @SerializedName("menu_items")
    public MenuItem[] menuItems;
    public Map<String, List<MenuItem>> menu;
    @SerializedName("p_number")
    public String phoneNumber;
    public String icon;

    @Override
    public Restaurant clone() {
        try {
            return (Restaurant) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }
}
