package com.misy.checkplease.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 2/6/15.
 */
public class Order implements Comparable<Order> {

    public enum State {
        @SerializedName("0")
        NEW,
        @SerializedName("1")
        HANDLED,
        @SerializedName("2")
        ARCHIVED
    }

    public String id;

    @SerializedName("restaurant_id")
    public String restaurantId;

    @SerializedName("table_num")
    public String tableNumber;

    public State state = State.NEW;

    public InstantService service;

    @SerializedName("menu_items")
    public MenuItem[] menuItems;

    private long date;

    @Override
    public int compareTo(@NonNull Order another) {
        if (this.state == another.state) {
            return 0;
        }

        return this.state.ordinal() > another.state.ordinal() ? 1 : -1;
    }

    public long getDate() {
        return date * 1000;
    }
}
