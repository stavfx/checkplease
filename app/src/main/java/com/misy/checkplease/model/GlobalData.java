package com.misy.checkplease.model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.misy.checkplease.api.auth.ConnectedResponse;

import java.util.ArrayList;

/**
 * Created on 19/5/15.
 */
public class GlobalData {
    public static final String ACTION_MANAGED_RESTAURANT_UPDATED = "ManagedRestaurantUpdated";
    public static final String ACTION_USER_LOGGED_IN = "UserLoggedIn";

    private static final String SHARED_PREFS_NAME = "GlobalDataSharedPrefs";
    private static final String KEY_CURRENT_USER = "KEY_CURRENT_USER_ID";
    private static final String KEY_MANAGED_RESTAURANT = "KEY_MANAGED_RESTAURANT";
    private static final String KEY_ALL_RESTAURANTS = "KEY_ALL_RESTAURANTS";
    private static final String KEY_ALL_CITIES = "KEY_ALL_CITIES";
    private static final String KEY_ALL_CATEGORIES = "KEY_ALL_CATEGORIES";

    private static GlobalData instance;

    private Context context;
    private SharedPreferences prefs;

    private String currentRestaurantID;
    private String tableNumber;
    private ArrayList<MenuItem> pendingOrder = new ArrayList<>();
    private ArrayList<Order> completedOrders = new ArrayList<>();

    public static GlobalData get(Context context) {
        if (instance == null) {
            instance = new GlobalData(context);
        }
        return instance;
    }

    private GlobalData(Context context) {
        this.context = context.getApplicationContext();
        prefs = this.context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public Restaurant getCurrentRestaurant() {
        return findRestaurantById(getCurrentRestaurantID());
    }

    public Restaurant findRestaurantById(String id) {
        for (Restaurant restaurant : getAllRestaurants()) {
            if (restaurant.id.equals(id)) {
                return restaurant;
            }
        }
        return null;
    }

    public String getCurrentRestaurantID() {
        return currentRestaurantID;
    }

    public GlobalData setCurrentRestaurantID(String currentRestaurantID) {
        if (!currentRestaurantID.equals(getCurrentRestaurantID())) {
            tableNumber = null;
            pendingOrder.clear();
            completedOrders.clear();
            this.currentRestaurantID = currentRestaurantID;
        }
        return this;
    }

    public boolean isTableSet() {
        return !TextUtils.isEmpty(tableNumber);
    }

    public String getTableNumber() {
        return tableNumber;
    }

    public GlobalData setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
        return this;
    }

    public boolean isLoggedIn() {
        return !TextUtils.isEmpty(getCurrentUser().id);
    }

    public User getCurrentUser() {
        String json = prefs.getString(KEY_CURRENT_USER, "{}");
        return new Gson().fromJson(json, User.class);
    }

    public GlobalData setCurrentUser(User user) {
        if (user == null) {
            user = new User();
        }
        prefs.edit().putString(KEY_CURRENT_USER, new Gson().toJson(user)).apply();
        return this;
    }

    public Restaurant getManagedRestaurant() {
        String json = prefs.getString(KEY_MANAGED_RESTAURANT, "{}");
        return new Gson().fromJson(json, Restaurant.class);
    }

    public GlobalData setManagedRestaurant(Restaurant restaurant) {
        prefs.edit().putString(KEY_MANAGED_RESTAURANT, new Gson().toJson(restaurant)).apply();
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(ACTION_MANAGED_RESTAURANT_UPDATED));
        return this;
    }

    public void saveLoggedInResponse(ConnectedResponse response) {
        ConnectedResponse.ConnectedData data = response.getData();
        setCurrentUser(data.user);
        setManagedRestaurant(data.managedRestaurant);
        setAllRestaurants(data.restaurants);
        setAllCities(data.cities);
        setCategories(data.menu_categories);
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(ACTION_USER_LOGGED_IN));
    }

    public Restaurant[] getAllRestaurants() {
        String json = prefs.getString(KEY_ALL_RESTAURANTS, "[]");
        return new Gson().fromJson(json, Restaurant[].class);
    }

    public GlobalData setAllRestaurants(Restaurant[] restaurants) {
        prefs.edit().putString(KEY_ALL_RESTAURANTS, new Gson().toJson(restaurants)).apply();
        return this;
    }

    public String[] getAllCities() {
        String json = prefs.getString(KEY_ALL_CITIES, "");
        return new Gson().fromJson(json, String[].class);
    }

    public GlobalData setAllCities(String[] cities) {
        prefs.edit().putString(KEY_ALL_CITIES, new Gson().toJson(cities)).apply();
        return this;
    }

    public GlobalData setCategories(Category[] categories) {
        prefs.edit().putString(KEY_ALL_CATEGORIES, new Gson().toJson(categories)).apply();
        return this;
    }

    public Category[] getCategories() {
        String json = prefs.getString(KEY_ALL_CATEGORIES, "[]");
        return new Gson().fromJson(json, Category[].class);
    }

    public ArrayList<MenuItem> getPendingOrder() {
        return pendingOrder;
    }

    public ArrayList<Order> getCompletedOrders() {
        return completedOrders;
    }
}
