package com.misy.checkplease.model;

/**
 * Created on 13/6/15.
 */
public class Category {
    public String id;
    public int priority;

    public Category() {
    }

    public Category(String id, int priority) {
        this.id = id;
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Category) {
            Category another = (Category) o;
            return this.id.equals(another.id);
        }
        return false;
    }

    @Override
    public String toString() {
        return id;
    }
}
