package com.misy.checkplease.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.misy.checkplease.R;
import com.misy.checkplease.api.API;
import com.misy.checkplease.model.GlobalData;
import com.octo.android.robospice.GsonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;

/**
 * Created on 3/5/15.
 */
public abstract class BaseFragment extends Fragment {
    protected SpiceManager spiceManager = new SpiceManager(GsonSpringAndroidSpiceService.class);

    protected <T extends View> T findView(int resId) {
        return findView(getView(), resId);
    }

    protected <T extends View> T findView(View root, int resId) {
        if (root != null) {
            return (T) root.findViewById(resId);
        }
        return null;
    }

    public abstract String getNavigationTag();


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getMainActivity().setDrawerEnabled(hasNavigationDrawer());
    }

    @Override
    public void onStart() {
        super.onStart();
        spiceManager.start(getActivity());
    }

    @Override
    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();
    }

    protected API getApi() {
        return API.getInstance(spiceManager);
    }

    protected GlobalData getGlobalData() {
        return GlobalData.get(getActivity());
    }

    protected ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    protected void setTitle(String title) {
        getActionBar().setTitle(title);
    }

    protected boolean hasNavigationDrawer() {
        return true;
    }

    protected void navigate(BaseFragment nextFragment, boolean addToBackstack) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (addToBackstack) {
            ft.addToBackStack(nextFragment.getNavigationTag());
        }
        ft.replace(R.id.main_fragment_holder, nextFragment, nextFragment.getNavigationTag());
        ft.commit();
    }

    protected void navigate(BaseFragment nextFragment) {
        navigate(nextFragment, true);
    }

    protected MainActivity getMainActivity() {
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            return activity;
        }
        return null;
    }

    protected void showLoadingSpinner() {
        MainActivity activity = getMainActivity();
        if (activity != null) {
            activity.showLoadingSpinner();
        }
    }

    protected void hideLoadingSpinner() {
        MainActivity activity = getMainActivity();
        if (activity != null) {
            activity.hideLoadingSpinner();
        }
    }

    protected void showKeyboard(final EditText editText) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}
