package com.misy.checkplease.api;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

/**
 * Created on 9/5/15.
 */
public abstract class BaseRequest<RESULT extends BaseResponse> extends SpringAndroidSpiceRequest<RESULT> {
    public BaseRequest(Class<RESULT> clazz) {
        super(clazz);
    }

    public String createCacheKey() {
        return "";
    }

    @Override
    public final RESULT loadDataFromNetwork() throws Exception {
        RESULT response = doInBackground();

        if (response != null && !response.isSuccess()) {
            throw new SpiceException("Server returned error: " + response.getErrorMessage());
        }

        return response;
    }

    protected abstract RESULT doInBackground() throws Exception;
}
