package com.misy.checkplease.api;

import com.google.gson.JsonArray;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

/**
 * Created on 3/6/15.
 */
public class TestCookiesRequest extends SpringAndroidSpiceRequest<JsonArray> {
    public TestCookiesRequest() {
        super(JsonArray.class);
    }

    @Override
    public JsonArray loadDataFromNetwork() throws Exception {
        String url = String.format("%s/testCookies", API.BASE_URL);
        JsonArray response = getRestTemplate().getForObject(url, JsonArray.class);
        System.out.println("current cookies=> " + response.toString());
        return response;

    }
}
