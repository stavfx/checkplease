package com.misy.checkplease.api.auth;

import com.google.gson.annotations.SerializedName;
import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.model.User;

/**
 * Created on 19/5/15.
 */
public class RegisterRequest extends BaseRequest<ConnectedResponse> {
    private RequestParams params;

    public RegisterRequest(String userId, String password, String firstName, String lastName, User.Type type) {
        super(ConnectedResponse.class);
        params = new RequestParams(userId, password, firstName, lastName, type);
    }

    @Override
    protected ConnectedResponse doInBackground() throws Exception {
        String url = String.format("%s/register", API.BASE_URL);
        return getRestTemplate().postForObject(url, params, getResultType());
    }

    private static class RequestParams {
        private String id;
        private String password;
        @SerializedName("first_name")
        private String firstName;
        @SerializedName("last_name")
        private String lastName;
        private int type = User.Type.CUSTOMER.getId();

        public RequestParams(String userId, String password, String firstName, String lastName, User.Type type) {
            this.id = userId;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.type = type.getId();
        }
    }
}
