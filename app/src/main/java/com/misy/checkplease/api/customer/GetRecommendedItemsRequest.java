package com.misy.checkplease.api.customer;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.BaseResponse;

/**
 * Created on 13/6/15.
 */
public class GetRecommendedItemsRequest extends BaseRequest<GetRecommendedItemsRequest.Response> {
    private String restaurantId;

    public GetRecommendedItemsRequest(String restaurantId) {
        super(Response.class);
        this.restaurantId = restaurantId;
    }

    @Override
    protected Response doInBackground() throws Exception {
        String url = String.format("%s/getRecommended/%s", API.BASE_URL, restaurantId);
        return getRestTemplate().getForObject(url, getResultType());
    }

    public static class Response extends BaseResponse<String[]> {

    }
}
