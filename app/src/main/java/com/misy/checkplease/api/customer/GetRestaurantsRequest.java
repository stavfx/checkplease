package com.misy.checkplease.api.customer;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.BaseResponse;
import com.misy.checkplease.model.Restaurant;

import java.util.List;

/**
 * Created on 3/6/15.
 */
public class GetRestaurantsRequest extends BaseRequest<GetRestaurantsRequest.Response> {

    public GetRestaurantsRequest() {
        super(Response.class);
    }

    @Override
    protected Response doInBackground() throws Exception {
        String url = String.format("%s/restaurants", API.BASE_URL);
        return getRestTemplate().getForObject(url, getResultType());
    }

    public static class Response extends BaseResponse<List<Restaurant>> {

    }
}
