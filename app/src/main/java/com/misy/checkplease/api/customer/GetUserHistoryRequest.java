package com.misy.checkplease.api.customer;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.BaseResponse;
import com.misy.checkplease.model.Order;

/**
 * Created on 12/6/15.
 */
public class GetUserHistoryRequest extends BaseRequest<GetUserHistoryRequest.Response> {

    public GetUserHistoryRequest() {
        super(Response.class);
    }

    @Override
    protected Response doInBackground() throws Exception {
        String url = String.format("%s/orders/user/history", API.BASE_URL);
        return getRestTemplate().getForObject(url, getResultType());
    }

    public static class Response extends BaseResponse<Order[]> {

    }
}
