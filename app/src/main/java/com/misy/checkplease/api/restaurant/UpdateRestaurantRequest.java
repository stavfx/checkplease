package com.misy.checkplease.api.restaurant;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.BaseResponse;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.model.Restaurant;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2/6/15.
 */
public class UpdateRestaurantRequest extends BaseRequest<UpdateRestaurantRequest.Response> {
    private Restaurant params;

    public UpdateRestaurantRequest(Restaurant restaurant) {
        super(Response.class);
        restaurant = restaurant.clone();
        flattenMenu(restaurant);
        this.params = restaurant;
    }

    public static class Response extends BaseResponse<Restaurant> {

    }

    @Override
    protected Response doInBackground() throws Exception {
        String url = String.format("%s/restaurants", API.BASE_URL);
        HttpEntity<Restaurant> entity = new HttpEntity<>(params);
        Response response = getRestTemplate().exchange(url, HttpMethod.PUT, entity, getResultType()).getBody();
        return response;
    }

    private void flattenMenu(Restaurant restaurant) {
        if (restaurant.menu == null) {
            return;
        }
        List<MenuItem> allItems = new ArrayList<>();
        for (List<MenuItem> menuItems : restaurant.menu.values()) {
            allItems.addAll(menuItems);
        }
        if (allItems.size() > 0) {
            restaurant.menuItems = allItems.toArray(new MenuItem[allItems.size()]);
        }
        restaurant.menu = null;
    }
}
