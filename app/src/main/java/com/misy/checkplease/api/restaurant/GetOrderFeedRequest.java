package com.misy.checkplease.api.restaurant;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;

/**
 * Created on 6/6/15.
 */
public class GetOrderFeedRequest extends BaseRequest<FeedResponse> {
    public GetOrderFeedRequest() {
        super(FeedResponse.class);
    }

    @Override
    protected FeedResponse doInBackground() throws Exception {
        String url = String.format("%s/orders", API.BASE_URL);
        return getRestTemplate().getForObject(url, getResultType());
    }
}
