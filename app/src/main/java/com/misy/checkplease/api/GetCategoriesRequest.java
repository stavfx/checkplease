package com.misy.checkplease.api;

import com.misy.checkplease.model.Category;

/**
 * Created by stav.raviv on 6/9/2015.
 */
public class GetCategoriesRequest extends BaseRequest<GetCategoriesRequest.Response> {
    public static final String TAG = "GetCategoriesRequest";

    public GetCategoriesRequest() {
        super(Response.class);
    }

    @Override
    protected Response doInBackground() throws Exception {
        String url = String.format("%s/menuCategories", API.BASE_URL);
        return getRestTemplate().getForObject(url, getResultType());
    }

    public static class Response extends BaseResponse<Category[]> {
    }
}
