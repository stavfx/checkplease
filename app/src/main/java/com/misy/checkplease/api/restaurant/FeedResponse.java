package com.misy.checkplease.api.restaurant;

import com.misy.checkplease.api.BaseResponse;
import com.misy.checkplease.model.Order;

/**
 * Created on 7/6/15.
 */
public class FeedResponse extends BaseResponse<FeedResponse.Data> {
    public static class Data {
        public Order[] dishes_orders;
        public Order[] services_orders;
    }
}
