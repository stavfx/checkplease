package com.misy.checkplease.api.customer;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.BaseResponse;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.model.Order;

import java.util.ArrayList;

/**
 * Created on 10/6/15.
 */
public class OrderDishesRequest extends BaseRequest<OrderDishesRequest.Response> {
    private Order order;

    public OrderDishesRequest(String restaurantId, String tableNum, ArrayList<MenuItem> menuItems) {
        super(Response.class);

        order = new Order();
        order.restaurantId = restaurantId;
        order.tableNumber = tableNum;
        order.menuItems = menuItems.toArray(new MenuItem[menuItems.size()]);
    }

    public static class Response extends BaseResponse<Order> {

    }

    @Override
    protected Response doInBackground() throws Exception {
        String url = String.format("%s/orders/dishes", API.BASE_URL);
        return getRestTemplate().postForObject(url, order, getResultType());
    }
}
