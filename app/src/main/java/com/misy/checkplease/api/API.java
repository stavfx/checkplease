package com.misy.checkplease.api;

import com.misy.checkplease.api.auth.ConnectedResponse;
import com.misy.checkplease.api.auth.LoginRequest;
import com.misy.checkplease.api.auth.LogoutRequest;
import com.misy.checkplease.api.auth.RegisterRequest;
import com.misy.checkplease.api.auth.UpdateUserRequest;
import com.misy.checkplease.api.customer.GetRecommendedItemsRequest;
import com.misy.checkplease.api.customer.GetUserHistoryRequest;
import com.misy.checkplease.api.customer.OrderDishesRequest;
import com.misy.checkplease.api.customer.OrderServiceRequest;
import com.misy.checkplease.api.restaurant.ArchiveCompletedOrdersRequest;
import com.misy.checkplease.api.restaurant.FeedResponse;
import com.misy.checkplease.api.restaurant.GetOrderFeedRequest;
import com.misy.checkplease.api.restaurant.GetRestaurantHistoryRequest;
import com.misy.checkplease.api.customer.GetRestaurantsRequest;
import com.misy.checkplease.api.restaurant.UpdateOrderStateRequest;
import com.misy.checkplease.api.restaurant.UpdateRestaurantRequest;
import com.misy.checkplease.model.InstantService;
import com.misy.checkplease.model.MenuItem;
import com.misy.checkplease.model.Order;
import com.misy.checkplease.model.Restaurant;
import com.misy.checkplease.model.User;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;

/**
 * Created on 9/5/15.
 */
public class API {
    public static final String BASE_URL = "http://stavfx.com/api";

    private SpiceManager spiceManager;

    private API(SpiceManager spiceManager) {
        this.spiceManager = spiceManager;
    }

    public static API getInstance(SpiceManager spiceManager) {
        return new API(spiceManager);
    }

    private void execute(BaseRequest request, RequestListener listener, long cacheExpiryDuration) {
        String cacheKey = request.createCacheKey();
        spiceManager.execute(request, cacheKey, cacheExpiryDuration, listener);
    }

    private void execute(BaseRequest request, RequestListener listener) {
        execute(request, listener, DurationInMillis.ALWAYS_EXPIRED);
    }

    public void login(String userId, String password, RequestListener<ConnectedResponse> listener) {
        execute(new LoginRequest(userId, password), listener);
    }

    public void logout(RequestListener<GenericResponse> listener) {
        execute(new LogoutRequest(), listener);
    }

    public void register(String userId, String password, String firstName, String lastName, User.Type type, RequestListener<ConnectedResponse> listener) {
        execute(new RegisterRequest(userId, password, firstName, lastName, type), listener);
    }

    public void updateRestaurant(Restaurant restaurant, RequestListener<UpdateRestaurantRequest.Response> listener) {
        execute(new UpdateRestaurantRequest(restaurant), listener);
    }

    public void getAllRestaurants(RequestListener<GetRestaurantsRequest.Response> listener) {
        execute(new GetRestaurantsRequest(), listener);
    }

    public void getOrderFeed(RequestListener<FeedResponse> listener) {
        execute(new GetOrderFeedRequest(), listener);
    }

    public void getRestaurantHistory(RequestListener<FeedResponse> listener) {
        execute(new GetRestaurantHistoryRequest(), listener);
    }

    public void orderService(String restaurantId, String tableNum, InstantService service, RequestListener<GenericResponse> listener) {
        execute(new OrderServiceRequest(restaurantId, tableNum, service), listener);
    }

    public void orderDishes(String restaurantId, String tableNum, ArrayList<MenuItem> items, RequestListener<OrderDishesRequest.Response> listener) {
        execute(new OrderDishesRequest(restaurantId, tableNum, items), listener);
    }

    public void updateOrder(Order order, RequestListener<FeedResponse> listener) {
        execute(new UpdateOrderStateRequest(order), listener);
    }

    public void archiveCompletedOrders(RequestListener<FeedResponse> listener) {
        execute(new ArchiveCompletedOrdersRequest(), listener);
    }

    public void getCategories(RequestListener<GetCategoriesRequest.Response> listener) {
        execute(new GetCategoriesRequest(), listener);
    }

    public void getUserHistory(RequestListener<GetUserHistoryRequest.Response> listener) {
        execute(new GetUserHistoryRequest(), listener);
    }

    public void updateUser(User user, RequestListener<UpdateUserRequest.Response> listener) {
        execute(new UpdateUserRequest(user), listener);
    }

    public void getRecommendedItems(String restaurantId, RequestListener<GetRecommendedItemsRequest.Response> listener) {
        execute(new GetRecommendedItemsRequest(restaurantId), listener);
    }

    public void testCookies() {
        TestCookiesRequest request = new TestCookiesRequest();
        spiceManager.execute(request, "", DurationInMillis.ALWAYS_EXPIRED, null);
    }
}
