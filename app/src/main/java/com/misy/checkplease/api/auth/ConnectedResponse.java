package com.misy.checkplease.api.auth;

import com.google.gson.annotations.SerializedName;
import com.misy.checkplease.api.BaseResponse;
import com.misy.checkplease.model.Category;
import com.misy.checkplease.model.Restaurant;
import com.misy.checkplease.model.User;

/**
 * Created on 03/06/2015.
 */
public class ConnectedResponse extends BaseResponse<ConnectedResponse.ConnectedData> {
    public static class ConnectedData {
        public User user;
        @SerializedName("admin_of_restaurant")
        public Restaurant managedRestaurant;
        public Restaurant[] restaurants;
        public String[] cities;
        public Category[] menu_categories;
    }
}
