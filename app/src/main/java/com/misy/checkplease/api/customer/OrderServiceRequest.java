package com.misy.checkplease.api.customer;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.GenericResponse;
import com.misy.checkplease.model.InstantService;
import com.misy.checkplease.model.Order;

/**
 * Created on 6/6/15.
 */
public class OrderServiceRequest extends BaseRequest<GenericResponse> {
    private Order order;

    public OrderServiceRequest(String restaurantId, String tableNum, InstantService service) {
        super(GenericResponse.class);
        order = new Order();
        order.restaurantId = restaurantId;
        order.tableNumber = tableNum;
        order.service = service;
    }

    @Override
    protected GenericResponse doInBackground() throws Exception {
        String url = String.format("%s/orders/service", API.BASE_URL);
        return getRestTemplate().postForObject(url, order, getResultType());
    }
}
