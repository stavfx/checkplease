package com.misy.checkplease.api.restaurant;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.model.Order;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

/**
 * Created on 6/6/15.
 */
public class UpdateOrderStateRequest extends BaseRequest<FeedResponse> {
    private Order order;

    public UpdateOrderStateRequest(Order order) {
        super(FeedResponse.class);
        this.order = order;
    }

    @Override
    protected FeedResponse doInBackground() throws Exception {
        String url = String.format("%s/orders", API.BASE_URL);
        HttpEntity<Order> entity = new HttpEntity<>(order);
        FeedResponse response = getRestTemplate().exchange(url, HttpMethod.PUT, entity, getResultType()).getBody();
        return response;
    }
}
