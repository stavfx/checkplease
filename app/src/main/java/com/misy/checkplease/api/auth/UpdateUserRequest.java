package com.misy.checkplease.api.auth;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.BaseResponse;
import com.misy.checkplease.model.User;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

/**
 * Created on 12/6/15.
 */
public class UpdateUserRequest extends BaseRequest<UpdateUserRequest.Response> {
    private User user;

    public UpdateUserRequest(User user) {
        super(Response.class);
        this.user = user;
    }

    @Override
    protected Response doInBackground() throws Exception {
        String url = String.format("%s/user", API.BASE_URL);
        HttpEntity<User> entity = new HttpEntity<>(user);
        Response response = getRestTemplate().exchange(url, HttpMethod.PUT, entity, getResultType()).getBody();
        return response;
    }

    public static class Response extends BaseResponse<User> {

    }
}
