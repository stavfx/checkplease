package com.misy.checkplease.api.auth;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.api.GenericResponse;

/**
 * Created on 2/6/15.
 */
public class LogoutRequest extends BaseRequest<GenericResponse> {

    public LogoutRequest() {
        super(GenericResponse.class);
    }

    @Override
    protected GenericResponse doInBackground() throws Exception {
        String url = String.format("%s/logout", API.BASE_URL);
        return getRestTemplate().postForObject(url, new Object(), GenericResponse.class);
    }
}
