package com.misy.checkplease.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 19/5/15.
 */
public class BaseResponse<Data> {
    private Data data;
    private boolean success;
    @SerializedName("error_message")
    private String errorMessage;

    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
