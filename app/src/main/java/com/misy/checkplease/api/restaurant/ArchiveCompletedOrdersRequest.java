package com.misy.checkplease.api.restaurant;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;

/**
 * Created on 8/6/15.
 */
public class ArchiveCompletedOrdersRequest extends BaseRequest<FeedResponse> {

    public ArchiveCompletedOrdersRequest() {
        super(FeedResponse.class);
    }

    @Override
    protected FeedResponse doInBackground() throws Exception {
        String url = String.format("%s/orders/archive", API.BASE_URL);
        return getRestTemplate().getForObject(url, getResultType());
    }
}
