package com.misy.checkplease.api.auth;

import com.misy.checkplease.api.API;
import com.misy.checkplease.api.BaseRequest;
import com.misy.checkplease.model.User;

/**
 * Created on 9/5/15.
 */
public class LoginRequest extends BaseRequest<ConnectedResponse> {

    private LoginParams params;

    public LoginRequest(String userId, String password) {
        super(ConnectedResponse.class);
        params = new LoginParams(userId, password);
    }

    @Override
    protected ConnectedResponse doInBackground() throws Exception {
        String url = String.format("%s/login", API.BASE_URL);
        return getRestTemplate().postForObject(url, params, getResultType());
    }

    @Override
    public String createCacheKey() {
        return "login:" + params.id;
    }

    private static class LoginParams {
        private String id;
        private String password;

        public LoginParams(String userId, String password) {
            this.id = userId;
            this.password = password;
        }
    }
}
